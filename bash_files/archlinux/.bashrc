#!/bin/bash

alias auru="sudo aura -Akuax"
alias pacu="sudo pacman -Syu"

alias upgrade="pacu && auru"
