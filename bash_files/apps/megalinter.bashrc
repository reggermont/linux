#!/bin/bash

alias megalint='docker run --rm -v $(pwd):/app -e DEFAULT_WORKSPACE=/app --user $(id -u):$(id -g) oxsecurity/megalinter:v7'
