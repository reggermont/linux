#!/bin/bash

alias git='LANG=en_GB git'
alias ga="git add"
alias gamend="git commit --amend"
alias gb="git branch"
alias gc="git checkout"
alias gcb="git checkout -b"
alias gcm="git commit -m"
alias get_git_current_branch="git rev-parse --abbrev-ref HEAD || exit"
alias gf="git fetch --all"
alias gs="git status"
gitclean() {
  git branch -vv | grep ': gone]' | grep -v "\*" | awk '{ print $1; }' | xargs -r git branch -D
}
gph() {
  if [ -n "$1" ]; then remote="$1"; else remote=origin; fi
  git push -u "$remote" "$(get_git_current_branch)"
}
gphf() {
  if [ -n "$1" ]; then remote="$1"; else remote=origin; fi
  git push -f -u "$remote" "$(get_git_current_branch)" --no-verify
}
gpl() {
  if [ -n "$1" ]; then remote="$1"; else remote=origin; fi
  git pull "$remote" "$(get_git_current_branch)"
}
