#!/bin/bash

#
# Experimental instant mode breaks bash wrapping / overflow
# https://github.com/nvbn/thefuck/issues/1080
# eval "$(thefuck --alias --enable-experimental-instant-mode)"

eval "$(thefuck --alias)"
