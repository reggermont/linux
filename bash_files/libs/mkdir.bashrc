#!/bin/bash

mkcd() {
  if [ -z "$1" ]; then
    echo "Needs a path as an argument"
    return 1
  fi
  mkdir "$1" && cd "$1" || exit
}
