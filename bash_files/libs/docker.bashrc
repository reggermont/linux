#!/bin/bash

## Docker
alias dockereset="docker system prune -a --volumes"
alias dockerclean='docker container rm $(docker container ls -aq)'

## Docker compose
alias dc="docker compose"
