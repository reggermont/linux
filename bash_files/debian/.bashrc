#!/bin/bash

# Old aliases from the time I was using Debian

alias apti="sudo apt install"
alias aptu="sudo apt update && sudo apt upgrade"
dpki() {
	if [ -z "$1" ]; then
		echo "Needs a path as an argument"
		return 1
	fi
	sudo dpkg -i "$1" && rm "$1"
}
