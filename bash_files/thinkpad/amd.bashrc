#!/bin/bash

# Usually not optimal as it forces use of specific GPU, but literally no impact on laptops with no dedicated GPUs
export DISABLE_LAYER_AMD_SWITCHABLE_GRAPHICS_1=1
