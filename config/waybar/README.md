# Sway configuration folder

Template taken on the internet and modified a little bit to my needs.

Needs a lot of rework, not a fan at all of the result

## How to use

- `mv ~/.config/waybar ~/.config/waybar.bak`
- `cp -rp config/waybar ~/.config/waybar`

If you pull updates and wants changes automatically applied, run instead:

- `ln -s config/waybar ~/.config/waybar`
