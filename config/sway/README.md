# Sway configuration folder

## How to use

- `mv ~/.config/sway ~/.config/sway.bak`
- `cp -rp config/sway ~/.config/sway`

If you pull updates and wants changes automatically applied, run instead:

- `ln -s config/waybar ~/.config/waybar`
