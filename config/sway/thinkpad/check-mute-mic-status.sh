#!/bin/bash

# Needs additional lines in the /etc/sudoers file
#
# USER HOST = (root) NOPASSWD: /home/USER/.config/sway/thinkpad/mic-led.sh
# USER HOST = (root) NOPASSWD: /home/USER/.config/sway/thinkpad/mute-led.sh
#
# Replace USER and HOST with your system informations
#

if pactl get-source-mute @DEFAULT_SOURCE@ | grep -q 'Mute: yes'; then
  sudo "$HOME"/.config/sway/thinkpad/mic-led.sh 1
else
  sudo "$HOME"/.config/sway/thinkpad/mic-led.sh 0
fi

if pactl get-sink-mute @DEFAULT_SINK@ | grep -q 'Mute: yes'; then
  sudo "$HOME"/.config/sway/thinkpad/mute-led.sh 1
else
  sudo "$HOME"/.config/sway/thinkpad/mute-led.sh 0
fi
