#!/bin/bash

dunstctl set-paused true

BLANK='#00000000'
CLEAR='#ffffff22'
DEFAULT='#00ff00'
TEXT='#00ff00'
WRONG='#880000bb'
VERIFYING='#bb00bbbb'

DIRNAME=$(dirname "$0")
IMAGE_PATH="$DIRNAME"/../../../assets/reggermont.png

swaylock \
    --inside-ver-color $CLEAR \
    --ring-ver-color $VERIFYING \
    \
    --inside-wrong-color $CLEAR \
    --ring-wrong-color $WRONG \
    \
    --inside-color $BLANK \
    --ring-color $DEFAULT \
    --line-color $BLANK \
    --separator-color $DEFAULT \
    \
    --text-ver-color $TEXT \
    --text-wrong-color $TEXT \
    --layout-text-color $TEXT \
    --key-hl-color $WRONG \
    --bs-hl-color $WRONG \
    \
    --ignore-empty-password \
    --image "$IMAGE_PATH" \
    --scaling center \
    --color $BLANK

dunstctl set-paused false
