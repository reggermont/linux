#!/bin/bash

NOTIF_HEADER="Media control"

if [ "$1" = "toggle" ]; then
  playerctl play-pause
  dunstify "$NOTIF_HEADER" "Media is $(playerctl status)"
elif [ "$1" = "next" ]; then
  playerctl next
  dunstify "$NOTIF_HEADER" "Skipping to next media"
elif [ "$1" = "prev" ] || [ "$1" = "previous" ]; then
  playerctl previous
  dunstify "$NOTIF_HEADER" "Skipping to previous media"
else
  echo "Unknown parameter $1"
fi
