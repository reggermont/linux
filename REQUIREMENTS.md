# Apps and libs requirements

Following is an non-exhaustive app and lib list to be fully compatible with the provided configurations in this repository

Those can be switched in favor to alternatives or ignored, but some config files may need to be adapted / deleted.


## Table of Content

- [Table of Content](#table-of-content)
- [Basics](#basics)
  - [AUR packages installer - Aura](#aur-packages-installer---aura)
    - [Installation](#installation)
  - [Bluetooth](#bluetooth)
    - [Installation](#installation-1)
  - [Displays management](#displays-management)
    - [Installation](#installation-2)
    - [Configuration](#configuration)
  - [Fonts - noto-fonts](#fonts---noto-fonts)
    - [Installation](#installation-3)
  - [Media controls - playerctl](#media-controls---playerctl)
    - [Installation](#installation-4)
  - [Notifications - Dunst](#notifications---dunst)
    - [Installation](#installation-5)
  - [XDG Desktop Portals](#xdg-desktop-portals)
    - [Installation](#installation-6)
- [Apps](#apps)
  - [Direnv](#direnv)
    - [Installation](#installation-7)
  - [Dolphin](#dolphin)
    - [Installation](#installation-8)
    - [Configuration](#configuration-1)
  - [imv](#imv)
    - [Installation](#installation-9)
    - [Configuration](#configuration-2)
  - [Flameshot](#flameshot)
    - [Installation](#installation-10)
    - [Configuration](#configuration-3)
  - [Kcalc](#kcalc)
    - [Installation](#installation-11)
  - [Libre Office](#libre-office)
    - [Installation](#installation-12)
  - [Signal Desktop](#signal-desktop)
    - [Installation](#installation-13)


## Basics

### AUR packages installer - Aura

#### Installation

[Installation guide](https://github.com/fosskers/aura?tab=readme-ov-file#installation)


[Return to ToC](#table-of-content)


### Bluetooth

Blueman was causing issues, keeping the good old basic CLI solution

#### Installation

`sudo pacman -S bluez bluez-utils` \
`sudo systemctl enable bluetooth` \
`sudo systemctl start bluetooth`


[Return to ToC](#table-of-content)


### Displays management

#### Installation

- Dynamic output configuration: `sudo aura -A kanshi-git` \
- Output management GUI: `sudo aura -A nwg-displays` \


#### Configuration

Configuration is available at [config/kanshi/config](./config/kanshi/config)

To enable it, see [Sway's how to use](./config/sway/README.md#how-to-use): copy or symlink


[Return to ToC](#table-of-content)


### Fonts - noto-fonts

#### Installation

- Base: `sudo pacman -S noto-fonts`
- Chinese / Japenese / Korean: `sudo pacman -S noto-fonts-cjk`
- Emojis: `sudo pacman -S noto-fonts-emoji`
- Variants (semi-bold, extra-light ...): `sudo pacman -S noto-fonts-extra`


[Return to ToC](#table-of-content)


### Media controls - playerctl

Controlling media players that implement the MPRIS D-Bus Interface Specification ([repository](https://github.com/altdesktop/playerctl))


#### Installation

`sudo pacman -S playerctl` ([reference](https://archlinux.org/packages/?sort=&q=playerctl))

### Notifications - Dunst

Some apps will not work properly without a notification service


#### Installation

`sudo pacman -S dunst`


[Return to ToC](#table-of-content)


### XDG Desktop Portals

#### Installation

- Base mandatory portal: `sudo pacman -S xdg-desktop-portal` (if you're using [Flameshot](#flameshot), run `sudo pacman -S xdg-desktop-portal=1.14.6-1` instead)
- Other portals (needed for apps like [Flameshot](#flameshot)): `sudo pacman -S xdg-desktop-portal-gtk xdg-desktop-portal-wlr`


[Return to ToC](#table-of-content)

## Apps

### Direnv

Automatically loads envs in specific folders

#### Installation

`sudo pacman -S direnv` ([reference](https://archlinux.org/packages/extra/x86_64/direnv/))


### Dolphin

KDE Desktop File Manager

#### Installation

`sudo pacman -S dolphin archlinux-xdg-menu`

#### Configuration

`sudo ln -rs ~/linux/config/xdg-menus/ /etc/xdg/menus`


[Return to ToC](#table-of-content)

### imv

Lightweight image viewer


#### Installation

`sudo pacman -S imv`


#### Configuration

`ln -s config/imv ~/.config/imv`


[Return to ToC](#table-of-content)


### Flameshot

Featureful screen capture tool

[Arch Wiki Page](https://wiki.archlinux.org/title/Flameshot)


#### Installation

`sudo pacman -S flameshot`


#### Configuration

Needs all xdg desktop portals [listed above](#xdg-desktop-portals) installed and [official documentation](https://github.com/flameshot-org/flameshot/blob/master/docs/Sway%20and%20wlroots%20support.md) followed

Not specified yet in the documentation, but latest working `xdg-desktop-portal` version is `1.14.6-1`, see [issue](https://github.com/flameshot-org/flameshot/issues/3363#issuecomment-1752003580)


[Return to ToC](#table-of-content)


### Kcalc

Basic Calculator app


#### Installation

`sudo pacman -S kcalc`


[Return to ToC](#table-of-content)


### Libre Office

Full office suite

#### Installation

`sudo pacman -S libreoffice-fresh` for the dev branch (recommended), `sudo pacman -S libreoffice-still` for the stable branch


[Return to ToC](#table-of-content)


### Signal Desktop

Desktop version of the instant messaging service

#### Installation

`sudo pacman -S signal-desktop`


[Return to ToC](#table-of-content)
