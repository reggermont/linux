# BASH + ARCH + SWAY

Collection of bash aliases that I use to speed up my productivity (or make things to work) + Arch and Sway configuration files, with personal additions

- [Bash](#bash)
  - [How to use](#how-to-use)
- [Sway](#sway)
  - [How to use](#how-to-use-1)
- [License](#license)

## Bash

### How to use

The steps below assume that you're cloning the repo in a new folder `~/linux`, adapts according to your preferences

- Install `git`
- `git clone git@gitlab.com:reggermont/linux.git ~/linux`
- `mv ~/.bashrc ~/.bashrc.bak`
- `cp ~/linux/bash_files/.bashrc.sample ~/.bashrc`
- Edit the new `.bashrc` file according to your preferences
- `source ~/.bashrc`


## Sway

### How to use

Each config folder has a README.md file. Check the use guide for each:

- [Sway](./config/sway/README.md#how-to-use)
- [Waybar](./config/waybar/README.md#how-to-use)


## License

See [LICENSE](LICENSE)
